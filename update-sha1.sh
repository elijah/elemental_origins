#!/bin/env bash
#
# This script will update the server.properties file to have the correct
# value for resource-pack-sha1. It only makes sense to run on a minecraft server
#

DOWNLOAD_URL="https://0xacab.org/api/v4/projects/4688/jobs/artifacts/main/download?job=build_zip"
TMP_PATH="/tmp/resource-pack.zip"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
CONFIG_PATH="$SCRIPT_DIR/../../../server.properties"

if [ ! -f "$CONFIG_PATH" ]; then
  echo "Could not find server.properties file"
else
  curl "$DOWNLOAD_URL" > "$TMP_PATH"
  digest=`sha1sum "$TMP_PATH" | cut -f1 -d" "`
  sed --in-place 's/^resource-pack-sha1=.*/resource-pack-sha1=$digest/' "$CONFIG_PATH"
  echo "modified server.properties"
  echo "resource-pack-sha1=$digest"
fi

