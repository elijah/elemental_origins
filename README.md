Elemental Origins is a minecraft datapack that add a number of player character origins,
each one granting new abilities and special powers. These are designed to be super fun
to play, but can be a bit over powered in comparison to normal Minecraft.

The new origins include:

Dragons

* Wind Dragon
* Fire Dragon
* Water Dragon
* Earth Dragon

Mages

* Alchemist
* Druid
* Elementalist
* Necromancer
* Shadow Mage
* Warlock

Creatures:

* Gargoyle
* Werewolf

Fae:

* Quickling
* Pixie
* Naiad

Prerequisites
==========================================

This datapack requires Fabric loader, Fabric API mod, and Origins mod. These must be installed on both the client
and the server. The datapack itself need only be installed on the server.

Installing on a server
------------------------------------------

These instructions are for linux.

Install Java:

    # apt install openjdk-17-jre-headless

Create the user minecraft:

    # adduser minecraft

Install minecraft java server:

    # su -l minecraft
    > mkdir -p server/mods
    > cd server
    > wget https://piston-data.mojang.com/v1/objects/84194a2f286ef7c14ed7ce0090dba59902951553/server.jar

See https://www.minecraft.net/en-us/download/server for the latest version of server.jar

Install Fabric server launcher:

    > cd server
    > curl -OJ https://meta.fabricmc.net/v2/versions/loader/1.20.1/0.14.21/0.11.2/server/jar

See https://fabricmc.net/use/server/ to generate the right curl command for a different version.

Create a launcher script 'fabric-console.sh':

    #!/usr/bin/dash
    while :
    do
      java -Xmx2G -jar /home/minecraft/server/fabric-server-mc.1.20.1-loader.0.14.21-launcher.0.11.2.jar nogui
      sleep 10
    done

Install Fabric API mod:

    > cd server/mods
    > wget https://mediafilez.forgecdn.net/files/4600/565/fabric-api-0.84.0%2B1.20.1.jar

See https://www.curseforge.com/minecraft/mc-mods/fabric-api for different versions.

Install Origin mod:

    > cd server/mods
    > wget https://mediafilez.forgecdn.net/files/4609/386/Origins-1.20.1-1.10.0.jar

See https://www.curseforge.com/minecraft/mc-mods/origins for different versions.

Installing on the client
------------------------------------------

Install Minecraft Java Edition:

    > wget https://launcher.mojang.com/download/Minecraft.deb
    # apt install ./Minecraft.deb

For other operating systems, see https://www.minecraft.net/en-us/download

Install Fabric Loader for Minecraft Launcher:

    > wget https://maven.fabricmc.net/net/fabricmc/fabric-installer/0.11.2/fabric-installer-0.11.2.jar
    > java -jar fabric-installer-0.11.2.jar client

For other operating systems, see https://fabricmc.net/use/installer/

Install Fabric API Mod:

    > cd ~/.minecraft/mods
    > wget https://mediafilez.forgecdn.net/files/4600/565/fabric-api-0.84.0%2B1.20.1.jar

See https://www.curseforge.com/minecraft/mc-mods/fabric-api for different versions.

Install Origin mod:

    > cd ~/.minecraft/mods
    > wget https://mediafilez.forgecdn.net/files/4609/386/Origins-1.20.1-1.10.0.jar

See https://www.curseforge.com/minecraft/mc-mods/origins for different versions.


Installing the datapack on the server
==========================================

https://0xacab.org/api/v4/projects/4688/jobs/artifacts/main/download?job=build_zip


Put the contents of this repository in the datapacks directory for your world. For example:

    >
    > cd ~/.minecraft/saves/testworld/datapacks
    > git clone https://0xacab.org/elijah/elemental_origins.git
    cd elemental_origins
    ./

Then start minecraft.

If you do this on the server you do not need to do it on the client.

However, this datapack requires the Fabric, Fabric API mod, and the Origins mod installed on both the client and the server.

Developing the datapack
==========================================

    > cd ~/.minecraft/saves/testworld/datapacks
    > git clone https://0xacab.org/elijah/elemental_origins.git

Development Notes
==========================================

TODO:

* use origins:action_on_item_pickup when origins is available for 1.20.4.

Notes:

* 20 ticks is one second
* Target selectors are run in order, so do faster filters first (e.g. tag, distance).

Tutorials & Tips:

* Great command tutorials: https://minecraftcommands.github.io/wiki/
* Optimizing: https://www.reddit.com/r/MinecraftCommands/comments/w4vjs3/whenever_i_create_datapacks_i_sometimes_do/
              https://minecraftcommands.github.io/wiki/optimising
* Command generators: https://www.digminecraft.com/generators/index.php

Reference:

* ID lists: https://minecraftitemids.com/
* Target Selectors: https://minecraft.wiki/w/Target_selectors
* Effect List: https://www.digminecraft.com/lists/effect_list_pc.php
* All Sounds: https://www.digminecraft.com/lists/sound_list_pc.php
* Particles: https://www.gamergeeks.net/apps/minecraft/particle-dust-command-generator
* Damage Types: https://minecraft.wiki/w/Tag#Damage_types
* NBT for all entities: https://www.digminecraft.com/data_tags/index.php
* Minecraft defaults: https://www.curseforge.com/minecraft/texture-packs/vanilladefault
* Advancement Triggers: https://minecraft.wiki/w/Advancement/JSON_format

Origin:

* Resource bars: https://github.com/apace100/origins-fabric/tree/1.19/src/main/resources/assets/origins/textures/gui/community
