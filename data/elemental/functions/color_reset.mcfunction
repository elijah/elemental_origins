#
# This function is called on a delay to reset the colors that are given to
# mobs when they are the target of a spell.
#

# reset the color of all entities tagged blue but no longer have the slowness 5 effect
power revoke @e[tag=blue,nbt=!{ActiveEffects:[{Id:2,Amplifier:5b}]}] elemental:color/blue
tag @e[tag=blue,nbt=!{ActiveEffects:[{Id:2,Amplifier:5b}]}] remove blue