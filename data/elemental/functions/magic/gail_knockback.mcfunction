#
# launch a mob that got hit by gail
#

# summon a temporary marker at the zeropoint, offset in the direction from the mob to the player
execute as @s facing entity @p[tag=gail_origin] feet positioned 0 0 0 run summon marker ^ ^0.6 ^-1 {Tags:["direction"]}

# use this position to apply it to the mobs motion vector
data modify entity @s Motion set from entity @e[tag=direction,type=marker,limit=1] Pos

# cause damage to mob
damage @s 2 minecraft:fall

# cleanup
kill @e[tag=direction,type=marker]
