#
# called on the magic missile, causes it to move toward the nearest hostile mob
#

# summon a temporary marker at the zeropoint, offset in the direction from the missile to the mob

execute as @s at @s anchored feet facing entity @e[type=#elemental:hostile_mob,distance=0..10,sort=nearest,limit=1] eyes positioned 0 0 0 run summon marker ^ ^ ^1 {Tags:[direction_marker]}

# use the marker's position as the vector of the motion of the missile
data modify entity @s Motion set from entity @e[tag=direction_marker,limit=1] Pos

# cleanup
kill @e[tag=direction_marker]
