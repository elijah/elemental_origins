particle minecraft:cherry_leaves ~ ~ ~ 0.5 2 0.5 0 500
playsound minecraft:block.note_block.chime player @s ~ ~ ~ 1 1

# give whatever food type the player is holding
execute if data entity @s {SelectedItem:{id:"minecraft:bread"}} run give @s minecraft:bread 63
execute if data entity @s {SelectedItem:{id:"minecraft:cooked_porkchop"}} run give @s minecraft:cooked_porkchop 63
execute if data entity @s {SelectedItem:{id:"minecraft:cooked_cod"}} run give @s minecraft:cooked_cod 63
execute if data entity @s {SelectedItem:{id:"minecraft:cooked_salmon"}} run give @s minecraft:cooked_salmon 63
execute if data entity @s {SelectedItem:{id:"minecraft:cake"}} run give @s minecraft:cake 3
execute if data entity @s {SelectedItem:{id:"minecraft:cookie"}} run give @s minecraft:cookie 63
execute if data entity @s {SelectedItem:{id:"minecraft:cooked_beef"}} run give @s minecraft:cooked_beef 63
execute if data entity @s {SelectedItem:{id:"minecraft:cooked_chicken"}} run give @s minecraft:cooked_chicken 63
execute if data entity @s {SelectedItem:{id:"minecraft:baked_potato"}} run give @s minecraft:baked_potato 63
execute if data entity @s {SelectedItem:{id:"minecraft:pumpkin_pie"}} run give @s minecraft:pumpkin_pie 63
execute if data entity @s {SelectedItem:{id:"minecraft:cooked_rabbit"}} run give @s minecraft:cooked_rabbit 63
execute if data entity @s {SelectedItem:{id:"minecraft:cooked_mutton"}} run give @s minecraft:cooked_mutton 63
