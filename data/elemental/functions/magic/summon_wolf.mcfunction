particle minecraft:soul_fire_flame ~ ~ ~ 0 1 0 0.5 500
playsound entity.evoker.cast_spell player @s ~ ~ ~ 1 2
#playsound minecraft:entity.wolf.howl player @a[distance=..20] ~ ~ ~ 1 1

summon minecraft:wolf ~ ~ ~ {Tags:["summoned"],DeathLootTable:"minecraft:empty",PersistenceRequired:0,Silent:1}
data modify entity @e[type=minecraft:wolf,distance=0..2,limit=1] Owner set from entity @s UUID

#tag @e[type=minecraft:wolf,distance=..10,limit=1] add friend
#
#summon minecraft:wolf ^-1 ^1 ^3 {Tags:["summoned"],DeathLootTable:"minecraft:empty",PersistenceRequired:0}
#data modify entity @e[type=minecraft:wolf,tag=!friend,distance=..5,limit=1] Owner set from entity @s UUID
#tag @e[type=minecraft:wolf,distance=..10,limit=1] add friend
#
#summon minecraft:wolf ^1 ^1 ^3 {Tags:["summoned"],DeathLootTable:"minecraft:empty",PersistenceRequired:0}
#data modify entity @e[type=minecraft:wolf,tag=!friend,distance=..5,limit=1] Owner set from entity @s UUID
