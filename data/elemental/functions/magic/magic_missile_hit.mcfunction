#
# Magic Missile found its target!
# called on the missile entity itself, so @s is the missile
#

kill @s

summon firework_rocket ~ ~ ~ {LifeTime:0,FireworksItem:{id:firework_rocket,Count:1,tag:{Fireworks:{Flight:2,Explosions:[{Type:4,Flicker:0,Trail:0,Colors:[I;11743532],FadeColors:[I;15790320]}]}}}}

summon firework_rocket ~ ~ ~ {LifeTime:0,FireworksItem:{id:firework_rocket,Count:1,tag:{Fireworks:{Flight:2,Explosions:[{Type:4,Flicker:0,Trail:0,Colors:[I;12801229],FadeColors:[I;15790320]}]}}}}

summon firework_rocket ~ ~ ~ {LifeTime:0,FireworksItem:{id:firework_rocket,Count:1,tag:{Fireworks:{Flight:2,Explosions:[{Type:4,Flicker:0,Trail:0,Colors:[I;15790320],FadeColors:[I;11743532]}]}}}}

damage @e[type=#elemental:hostile_mob,distance=0..3,limit=1] 30 minecraft:magic by @p[tag=magic_missile_source,limit=1,sort=nearest]


