#
# do damage to an entity that is far from an exploding fireball
#

data merge entity @s {Fire:100}
damage @s 6 minecraft:fireball by @p[tag=fireball_source,limit=1,sort=nearest]
