#
# do damage to an entity that is close to an exploding fireball
#

# summon a temporary marker at the zeropoint, offset in the direction from the fireball to the mob
execute as @s facing entity @e[tag=exploding_fireball,distance=..5,limit=1] feet positioned 0 0 0 run summon marker ^ ^1.5 ^-1.5 {Tags:["direction"]}

# use this position vector to apply it to the mobs motion
data modify entity @s Motion set from entity @e[tag=direction,type=marker,limit=1] Pos

# cause damage to mob
data merge entity @s {Fire:200}
damage @s 18 minecraft:fireball by @p[tag=fireball_source,limit=1,sort=nearest]

# cleanup
kill @e[tag=direction,type=marker]
