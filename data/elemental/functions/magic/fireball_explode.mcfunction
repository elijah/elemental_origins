#
# Fireball go boom!
# called on the fireball entity itself, so @s is the fireball
#

tag @s add exploding_fireball
execute as @e[type=#elemental:hostile_mob,distance=0..4] at @s run function elemental:magic/fireball_close_damage
execute as @e[type=#elemental:hostile_mob,distance=0..8] at @s run function elemental:magic/fireball_far_damage

fill ~-1 ~-1 ~-1 ~1 ~1 ~1 minecraft:fire replace minecraft:air
particle minecraft:explosion ~ ~ ~ 1 1 1 0.1 3
particle minecraft:flame ~ ~ ~ 0 0 0 0.1 2000
playsound minecraft:entity.generic.explode player @p ~ ~ ~ 1 1

# the fireball source is tagged so we know who to attribute as the source of damage.
# doesn't work with simultaneous fireball casters
tag @p[tag=fireball_source,limit=1,sort=nearest] remove fireball_source

kill @s
