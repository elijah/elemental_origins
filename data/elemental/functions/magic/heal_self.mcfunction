effect give @s minecraft:instant_health 2 0
effect clear @s minecraft:poison
effect clear @s minecraft:wither
effect clear @s minecraft:weakness
effect clear @s minecraft:slowness
effect clear @s minecraft:unluck
effect clear @s minecraft:blindness
effect clear @s minecraft:hunger
effect clear @s minecraft:mining_fatigue
effect clear @s minecraft:bad_omen

#effect give @s minecraft:saturation 1 1
particle minecraft:heart ~ ~ ~ 1 1 1 0 20
playsound minecraft:entity.ender_eye.death player @a[distance=..10] ~ ~ ~ 10 0.3 1