team add mobs

execute at @s as @e[distance=0..32,type=minecraft:spider] run team join mobs @s
execute at @s as @e[distance=0..32,type=minecraft:zombie] run team join mobs @s
execute at @s as @e[distance=0..32,type=minecraft:slime] run team join mobs @s

execute at @e[tag=summoned,distance=1..25] run summon minecraft:snowball ~ ~1.25 ~ {Tags:['make_agro_snowball'],Motion:[0.0,-1.0,0.0],damage:0}

execute as @e[tag=make_agro_snowball] run data modify entity @s Owner set from entity @e[team=mobs,sort=random,limit=1,distance=0..25] UUID
