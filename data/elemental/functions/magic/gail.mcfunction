#
# launch and damage all the hostile mobs that are near self.
#

playsound minecraft:entity.ender_dragon.flap master @e[type=minecraft:player,distance=..35]
particle minecraft:ash ~ ~ ~ 8 8 8 0.55 1500
tag @s add gail_origin
execute as @e[distance=..20,type=#elemental:hostile_mob] at @s run function elemental:magic/gail_knockback
tag @s remove gail_origin
