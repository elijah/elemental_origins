#
# run on the target of a ice bolt
#

playsound minecraft:entity.player.hurt_freeze player @p ~ ~ ~ 2 1 1
particle dust 0.84 0.99 1 1 ~ ~ ~ 0.5 1 0.5 1 500 normal

# 5 seconds of slowness, amplitude 5
effect give @s minecraft:slowness 5 5 true

# a bit of ice damage
# (this is done in the power, so it can be a bientity action and the damage comes from the caster)
# damage @s 10 minecraft:freeze

# put some snow on the ground (but not the air)
# disabled because it is a little buggy, sometimes erases blocks
#execute unless block ~ ~-1 ~ minecraft:air run fill ~ ~ ~ ~ ~ ~ minecraft:snow[layers=1]

# make the target blue & reset the color when slowness wears off
# note: scheduled time must be at least one tick greater than the duration of the effect
power grant @s elemental:color/blue
tag @s add blue
schedule function elemental:color_reset 101 append
