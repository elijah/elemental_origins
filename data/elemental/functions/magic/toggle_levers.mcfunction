playsound minecraft:block.amethyst_block.break player @s

# north
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 barrier replace lever[face=wall,powered=false,facing=north]
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 lever[face=wall,powered=false,facing=north] replace lever[face=wall,powered=true,facing=north]
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 lever[face=wall,powered=true,facing=north] replace barrier

# south
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 barrier replace lever[face=wall,powered=false,facing=south]
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 lever[face=wall,powered=false,facing=south] replace lever[face=wall,powered=true,facing=south]
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 lever[face=wall,powered=true,facing=south] replace barrier

# east
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 barrier replace lever[face=wall,powered=false,facing=east]
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 lever[face=wall,powered=false,facing=east] replace lever[face=wall,powered=true,facing=east]
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 lever[face=wall,powered=true,facing=east] replace barrier

# west
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 barrier replace lever[face=wall,powered=false,facing=west]
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 lever[face=wall,powered=false,facing=west] replace lever[face=wall,powered=true,facing=west]
fill ~-10 ~-10 ~-10 ~10 ~10 ~10 lever[face=wall,powered=true,facing=west] replace barrier
