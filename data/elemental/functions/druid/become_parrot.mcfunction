title @s actionbar {"text":"Parrot","bold":true,"color":"aqua"}

execute in minecraft:overworld run function elemental:inventory/stash_armor

origin set @s origins:origin elemental:wild_shape/parrot

#playsound minecraft:entity.parrot.ambient player @s
particle minecraft:explosion ~ ~ ~ 0 1 0 2 1

item replace entity @s armor.head with minecraft:player_head{WildShapeParrot:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}], SkullOwner:{Id:[I;-1275253262,-1956560244,-1243401661,1024939448],Properties:{textures:[{Value:'eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2JhOTkzMjhkY2NkYWVhOGZhZTNlZmRiZmQxYzAwZDkwNDc4MGNhNTc5MGM1ZGQzOThhM2RkNGQxOWM5ZDhlOCJ9fX0='}]}}}

item replace entity @s armor.chest with minecraft:leather_chestplate{WildShapeParrot:1b, display:{color:383738}, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]}

item replace entity @s armor.legs with minecraft:leather_leggings{WildShapeParrot:1b, display:{color:383738}, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]}

item replace entity @s armor.feet with minecraft:leather_boots{WildShapeParrot:1b, display:{color:383738}, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]}
