title @s actionbar {"text":"Turtle","bold":true,"color":"green"}

origin set @s origins:origin elemental:wild_shape/turtle

#playsound minecraft:entity.player.swim player @s
particle minecraft:explosion ~ ~ ~ 0 1 0 2 1

item replace entity @s armor.head with minecraft:player_head{WildShapeTurtle:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}], display:{Name:'{"text":"Sea Turtle"}'}, SkullOwner:{Id:[I;610214580,746343068,-2030395706,1280632058],Properties:{textures:[{Value:'eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMGE0MDUwZTdhYWNjNDUzOTIwMjY1OGZkYzMzOWRkMTgyZDdlMzIyZjlmYmNjNGQ1Zjk5YjU3MThhIn19fQ=='}]}}}

item replace entity @s armor.chest with minecraft:leather_chestplate{WildShapeTurtle:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}], display:{Name:'{"text":"Sea Turtle"}', color:43520}}

item replace entity @s armor.legs with minecraft:leather_leggings{WildShapeTurtle:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}], display:{Name:'{"text":"Sea Turtle"}', color:43520}}

item replace entity @s armor.feet with minecraft:leather_boots{WildShapeTurtle:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}], display:{Name:'{"text":"Sea Turtle"}', color:43520}}

