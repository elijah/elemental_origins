title @s actionbar {"text":"Bear","bold":true,"color":"white"}

origin set @s origins:origin elemental:wild_shape/bear

#playsound minecraft:entity.polar_bear.ambient player @s
particle minecraft:explosion ~ ~ ~ 0 1 0 2 10

item replace entity @s armor.head with minecraft:player_head{WildShapeBear:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}], SkullOwner:{Id:[I;-1665196768,1405243216,-1661051137,887436813],Properties:{textures:[{Value:'eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmFiMTc4ZjVjZGQ3NTBmMGUzNTY4NjBhYTU1MzkxNTNlYjJhYmVjMWUxNDZjYTU3YzY1ZDI1YTVkZjhmZGZlIn19fQ=='}]}}}

item replace entity @s armor.chest with minecraft:leather_chestplate{display:{color:16777215}, WildShapeBear:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]}

item replace entity @s armor.legs with minecraft:leather_leggings{display:{color:16777215}, WildShapeBear:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]}

item replace entity @s armor.feet with minecraft:leather_boots{display:{color:16777215}, WildShapeBear:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]}
