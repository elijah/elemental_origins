title @s actionbar {"text":"Dolphin","bold":true,"color":"gray"}

origin set @s origins:origin elemental:wild_shape/dolphin

#playsound minecraft:entity.parrot.ambient player @s
particle minecraft:explosion ~ ~ ~ 0 1 0 2 1

item replace entity @s armor.head with minecraft:player_head{WildShapeDolphin:1b, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}], SkullOwner:{Id:[I;-364683052,-1946074556,-1560564271,-425995975],Properties:{textures:[{Value:'eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTU0NjI0ZmUwZTBiMjBmNmZlNjdhMDY4ZWJjMmY1ODhmNjlmZDQ0MmI1NTUzNjhjZDRiNjZmNGFhYmE2MzE3NSJ9fX0='}]}}}

item replace entity @s armor.chest with minecraft:leather_chestplate{WildShapeDolphin:1b, display:{color:14606046}, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]}

item replace entity @s armor.legs with minecraft:leather_leggings{WildShapeDolphin:1b, display:{color:14606046}, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]}

item replace entity @s armor.feet with minecraft:leather_boots{WildShapeDolphin:1b, display:{color:14606046}, Unbreakable:1, Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]}
