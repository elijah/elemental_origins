title @s actionbar {"text":"Druid","bold":true,"color":"dark_red"}

#playsound minecraft:entity.player.levelup player @s
particle minecraft:explosion ~ ~ ~ 0 1 0 2 1

clear @s minecraft:player_head{WildShapeBear:1b}
clear @s minecraft:leather_chestplate{WildShapeBear:1b}
clear @s minecraft:leather_leggings{WildShapeBear:1b}
clear @s minecraft:leather_boots{WildShapeBear:1b}

execute in minecraft:overworld run function elemental:inventory/unstash_armor
origin set @s origins:origin elemental:druid