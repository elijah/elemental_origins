scoreboard players set * counts 0
execute as @e[type=minecraft:skeleton] run scoreboard players add skeletons counts 1
execute as @e[type=minecraft:wither_skeleton] run scoreboard players add skeletons counts 1
execute as @e[tag=minion] run scoreboard players add minions counts 1
execute as @e[team=undead] run scoreboard players add undead counts 1
execute as @e[team=enemies] run scoreboard players add enemies counts 1
