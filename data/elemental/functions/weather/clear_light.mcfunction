#
# this function will clear the invisible lights that are around the entities tagged 'clear_light'.
# these entities are typically invisible armor stands summoned to mark the areas where lights
# have been dropped.
#
execute at @e[tag=light_marker] run fill ~ ~ ~ ~ ~ ~ minecraft:air replace minecraft:light
kill @e[tag=light_marker]