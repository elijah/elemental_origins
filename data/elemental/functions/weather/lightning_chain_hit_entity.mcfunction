# this is called on the target when they are hit with a lighting bolt.
# it applies the sound, damage, and then gives them the power lightning_chain_relay
# so that the bolt can arc to another mob

# thunder!
playsound minecraft:entity.lightning_bolt.thunder player @p[distance=..36] ~ ~ ~ 2 2 1

# sparks!
particle minecraft:electric_spark ~ ~ ~ 0.5 0.5 0.5 1 1000

# don't hit player characters
execute if entity @e[type=!player] run damage @s 16 minecraft:lightning_bolt by @p[tag=lightning_caster,sort=nearest,limit=1]

# don't damage ourselves! (lightning can arc in a way where caster gets hit)
#execute if entity @s[tag=!lightning_caster] run damage @s 16 minecraft:lightning_bolt

# grant this entity the power to relay the lightning
power grant @s elemental:weather/lightning_chain_relay

# drop a light because lightning makes light!
setblock ~ ~1 ~ minecraft:light[level=15] keep

# drop a marker on top of the light
summon minecraft:marker ~ ~1 ~ {Tags:["light_marker"]}

# schedule the destruction of the minecraft:light block.
schedule function elemental:weather/clear_light 8 replace