# this is called when the lightning misses and hits a block

# sparks!
particle minecraft:electric_spark ~ ~ ~ 0.5 0.5 0.5 1 1000

# drop a light because lightning makes light!
setblock ~ ~1 ~ minecraft:light[level=15] keep

# drop a marker on top of the light
summon minecraft:marker ~ ~1 ~ {Tags:["light_marker"]}

# schedule the destruction of the minecraft:light block.
schedule function elemental:weather/clear_light 8 replace
