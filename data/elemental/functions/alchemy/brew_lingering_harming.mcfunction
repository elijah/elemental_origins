clear @s minecraft:experience_bottle
give @s minecraft:dragon_breath

give @s minecraft:fermented_spider_eye
give @s minecraft:glistering_melon_slice
give @s minecraft:lingering_potion{Potion:"minecraft:strong_harming"}

advancement revoke @s only elemental:alchemy/brew_lingering_harming
