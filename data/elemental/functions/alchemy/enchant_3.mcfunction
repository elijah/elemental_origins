# recover levels used
experience add @s 3 levels

# recover some of the lapis used
give @s minecraft:lapis_lazuli 2

# revoke the advancement that triggered this function,
# so it can be triggered again
advancement revoke @s only elemental:alchemy/enchant_3
