clear @s minecraft:experience_bottle

give @s minecraft:golden_apple

# 22 = absorption

give @s minecraft:potion{CustomPotionEffects:[{Id:22,Amplifier:2,Duration:12000,ShowParticles:0b}],CustomPotionColor:16055075,display:{Name:"\"Potion of Vigor\""}}

advancement revoke @s only elemental:alchemy/brew_vigor
