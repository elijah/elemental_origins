clear @s minecraft:experience_bottle

give @s minecraft:sugar

# 3 = haste (mining & attack speed)
# 1 = speed (movement speed)
# 8 = jump boost

give @p minecraft:potion{CustomPotionEffects:[{Id:1,Amplifier:3,Duration:2400},{Id:3,Amplifier:1,Duration:2400},{Id:8,Amplifier:3,Duration:2400}],CustomPotionColor:5111803,display:{Name:"\"Potion of Speed\""}}

#give @s minecraft:potion{Potion:"minecraft:strong_swiftness"}

advancement revoke @s only elemental:alchemy/brew_swiftness
