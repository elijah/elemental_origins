clear @s minecraft:experience_bottle
give @s minecraft:dragon_breath

give @s minecraft:ghast_tear

# 10 = regeneration
# 23 = saturation

give @s minecraft:lingering_potion{CustomPotionEffects:[{Id:10,Amplifier:1,Duration:1800},{Id:23,Amplifier:4,Duration:1800}],CustomPotionColor:13378731,display:{Name:"\"Potion of Regeneration\""}}

advancement revoke @s only elemental:alchemy/brew_lingering_regeneration
