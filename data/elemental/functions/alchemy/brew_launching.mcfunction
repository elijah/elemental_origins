clear @s minecraft:experience_bottle

give @s minecraft:firework_rocket
give @p minecraft:potion{CustomPotionEffects:[{Id:25,Amplifier:100,Duration:15}],CustomPotionColor:9704500,display:{Name:"\"Potion of Launching\""}}

advancement revoke @s only elemental:alchemy/brew_launching
