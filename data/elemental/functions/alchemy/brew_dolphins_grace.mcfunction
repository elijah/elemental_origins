clear @s minecraft:experience_bottle

give @s minecraft:prismarine_shard

# 29 = conduit power (underwater visiblity, mining speed, breathing)
# 30 = dolphin's grace (swimming speed)
# 13 = water breathing

give @s minecraft:potion{CustomPotionEffects:[{Id:13,Duration:3600},{Id:29,Duration:3600},{Id:30,Duration:3600}],CustomPotionColor:267439,display:{Name:"\"Potion of Dolphin Friends\""}}

advancement revoke @s only elemental:alchemy/brew_dolphins_grace
