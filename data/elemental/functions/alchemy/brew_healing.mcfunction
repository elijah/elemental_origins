clear @s minecraft:experience_bottle

give @s minecraft:glistering_melon_slice
give @s minecraft:potion{Potion:"minecraft:strong_healing"} 1

advancement revoke @s only elemental:alchemy/brew_healing
