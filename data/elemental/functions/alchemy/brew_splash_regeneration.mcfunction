clear @s minecraft:experience_bottle
give @s minecraft:gunpowder

give @s minecraft:ghast_tear
give @s minecraft:splash_potion{CustomPotionEffects:[{Id:10,Amplifier:1,Duration:1800},{Id:23,Amplifier:4,Duration:1800}],CustomPotionColor:13378731,display:{Name:"\"Potion of Regeneration\""}}

advancement revoke @s only elemental:alchemy/brew_splash_regeneration
