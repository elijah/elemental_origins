clear @s minecraft:experience_bottle

give @s minecraft:glow_ink_sac
give @s minecraft:potion{CustomPotionEffects:[{Id:24,Duration:3600}],CustomPotionColor:8781055,display:{Name:"\"Potion of Glowing\""}}

advancement revoke @s only elemental:alchemy/brew_glowing
