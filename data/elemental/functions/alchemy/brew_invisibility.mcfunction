clear @s minecraft:experience_bottle

give @s minecraft:golden_carrot
give @s minecraft:fermented_spider_eye
give @s minecraft:potion{Potion:"minecraft:long_invisibility"}

advancement revoke @s only elemental:alchemy/brew_invisibility
