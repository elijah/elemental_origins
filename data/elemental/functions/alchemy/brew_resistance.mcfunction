clear @s minecraft:experience_bottle

give @s minecraft:nautilus_shell

# 11 = resistance
# 12 = fire resistance

give @s minecraft:potion{CustomPotionEffects:[{Id:11,Amplifier:1,Duration:12000},{Id:12,Duration:12000}],CustomPotionColor:10816510,display:{Name:"\"Potion of Resistance\""}}

advancement revoke @s only elemental:alchemy/brew_resistance


