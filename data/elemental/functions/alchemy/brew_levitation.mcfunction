clear @s minecraft:experience_bottle

give @s minecraft:shulker_shell

# 25 = levitation
# 28 = slow falling

give @p minecraft:potion{CustomPotionEffects:[{Id:25,Amplifier:2,Duration:400},{Id:28,Duration:800}],CustomPotionColor:12105912,display:{Name:"\"Potion of Defying Gravity\""}}

advancement revoke @s only elemental:alchemy/brew_levitation
