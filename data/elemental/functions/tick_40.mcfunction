###
### this function will run once every 40 ticks
###

#
# give magic item to players who should have them
#

# shadow blade
execute as @p[tag=shadow_blade_owner,sort=random,limit=1] unless data entity @s {Inventory:[{tag:{ShadowBlade:1b}}]} run give @s netherite_sword{ShadowBlade:1b, display:{Name:'{"text":"Shadow Blade","color":"blue","italic":"true"}'}, Unbreakable:1, Enchantments:[{id:sharpness,lvl:5},{id:smite,lvl:5},{id:bane_of_arthropods,lvl:5},{id:fire_aspect,lvl:2},{id:knockback,lvl:2},{id:looting,lvl:3},{id:sweeping_edge,lvl:3}]}

# shadow armor
execute as @p[tag=shadow_armor_owner,sort=random,limit=1] unless data entity @s {Inventory:[{tag:{ShadowArmor:1b}}]} run item replace entity @s armor.chest with netherite_chestplate{ShadowArmor:1b, display:{Name:'{"text":"Shadow Armor","color":"blue","italic":"true"}'}, Unbreakable:1, Enchantments:[{id:protection,lvl:4},{id:blast_protection,lvl:4},{id:fire_protection,lvl:4},{id:projectile_protection,lvl:4}]}

# bow
execute as @p[tag=fae_bow_owner,sort=random,limit=1] unless data entity @s {Inventory:[{tag:{FaeBow:1b}}]} run give @s minecraft:bow{FaeBow:1b, display:{Name:'{"text":"Faerie Bow","color":"aqua","italic":"true"}'}, Unbreakable:1, Enchantments:[{id:infinity,lvl:1},{id:punch,lvl:2},{id:flame,lvl:1}]}

execute as @p[tag=fae_bow_owner,sort=random,limit=1] unless data entity @s {Inventory:[{id:"minecraft:spectral_arrow"}]} run give @s minecraft:spectral_arrow

# pickaxe
execute as @p[tag=fae_pickaxe_owner,sort=random,limit=1] unless data entity @s {Inventory:[{tag:{FaePickaxe:1b}}]} run give @s minecraft:diamond_pickaxe{FaePickaxe:1b, display:{Name:'{"text":"Faerie Pickaxe","color":"aqua","italic":"true"}'}, Unbreakable:1, Enchantments:[{id:efficiency,lvl:5},{id:fortune,lvl:3}]}

# trident (disabled because it creates too many tridents)
execute as @p[tag=fae_trident_owner,sort=random,limit=1] unless data entity @s {Inventory:[{tag:{FaeTrident:1b}}]} run give @s minecraft:trident{FaeTrident:1b, display:{Name:'{"text":"Faerie Trident","color":"aqua","italic":"true"}'}, Unbreakable:1, Enchantments:[{id:channeling,lvl:1},{id:impaling,lvl:5},{id:loyalty,lvl:3}]}

#
# destroy any magic item held by a player who should not have it
#

clear @p[tag=!shadow_blade_owner] minecraft:netherite_sword{ShadowBlade:1b}
clear @p[tag=!shadow_armor_owner] minecraft:netherite_chestplate{ShadowArmor:1b}
clear @p[tag=!fae_bow_owner] minecraft:bow{FaeBow:1b}
clear @p[tag=!fae_pickaxe_owner] minecraft:diamond_pickaxe{FaePickaxe:1b}
clear @p[tag=!fae_trident_owner] minecraft:trident{FaeTrident:1b}
