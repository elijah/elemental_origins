particle minecraft:sweep_attack ~ ~ ~ 0 1 0 1 100
effect give @e[distance=..2,team=!undead] minecraft:slowness 60 4
effect give @e[distance=..2,team=!undead] minecraft:weakness 60 4
particle minecraft:mycelium ~ ~ ~ 1 1 1 1 1000
playsound minecraft:entity.player.hurt_sweet_berry_bush player @a[distance=..20] ~ ~ ~ 100 2