#
# Called when a mob attacks the player or is attacked by the player. @s is the mob.
#
# This is a litte confusing: it creates a snowball, then makes the mob the owner of the snowball,
# making the minions aggressive toward the mob.
#
tag @s add enemy_mob
execute at @e[type=!minecraft:player,team=necromancer,distance=..64] run summon minecraft:snowball ~ ~1.5 ~ {Tags:["initialize_aggression"],Motion:[0.0,-1.0,0.0]}
execute as @e[tag=initialize_aggression] run data modify entity @s Owner set from entity @e[tag=enemy_mob,limit=1] UUID
tag @s remove enemy_mob
tag @e remove initialize_agression
