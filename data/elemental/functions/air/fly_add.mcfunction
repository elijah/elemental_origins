# called when the player gains the ability to fly

power grant @s elemental:air/creative_flight elemental:air/fly_timer
particle minecraft:cloud ~ ~ ~ 0.5 1 0.5 .2 300
playsound entity.evoker.cast_spell player @s ~ ~ ~ 1 2
