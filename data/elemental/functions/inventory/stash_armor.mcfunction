#
# removes the player's armor and stashes on an hidden armor stand
#
# NOTE: this should be run as
#
#    execute in minecraft:overworld run \
#       function elemental:inventory/stash_armor
#
# Otherwise, the armor will be lost when the player changes dimensions
#

# give @s a player_id if they don't have one yet
function elemental:inventory/assign_player_id

# summon armor stand
summon armor_stand 0 0 0 {Invisible:1b,Tags:[armor_stash,current_stash]}

# link @s and the armor stand using the custom score player_id
scoreboard players operation @e[type=armor_stand,limit=1,tag=current_stash] player_id = @s player_id

# copy armor to the armor stand

# copy feet
item replace entity @e[type=armor_stand,limit=1,tag=current_stash] armor.feet from entity @s armor.feet

# copy legs
item replace entity @e[type=armor_stand,limit=1,tag=current_stash] armor.legs from entity @s armor.legs

# copy chest
item replace entity @e[type=armor_stand,limit=1,tag=current_stash] armor.chest from entity @s armor.chest

# copy head
item replace entity @e[type=armor_stand,limit=1,tag=current_stash] armor.head from entity @s armor.head

tag @e[type=armor_stand] remove current_stash

# remove armor from the player
item replace entity @s armor.head with minecraft:air
item replace entity @s armor.chest with minecraft:air
item replace entity @s armor.legs with minecraft:air
item replace entity @s armor.feet with minecraft:air

