#
# Saves the currently selected item, then restores it 1 tick later.
#
# In effect, it allows the item to be used again even if it is consumed,
# because minecraft handles consumption between this function being called
# and the scheduled function being called.
#

function elemental:inventory/stash_item
schedule function elemental:inventory/unstash_item 1 append
