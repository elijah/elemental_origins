#
# restores the player's armor that has been previously stashed
#

tag @s add me

# add the tag 'chosen' to the armor stand that is linked
# to the player through the player_id score
execute as @e[type=armor_stand,tag=armor_stash] if score @s player_id = @p[limit=1,tag=me] player_id run tag @s add chosen

item replace entity @s armor.feet from entity @e[type=armor_stand,tag=chosen,limit=1] armor.feet
item replace entity @s armor.chest from entity @e[type=armor_stand,tag=chosen,limit=1] armor.chest
item replace entity @s armor.legs from entity @e[type=armor_stand,tag=chosen,limit=1] armor.legs
item replace entity @s armor.head from entity @e[type=armor_stand,tag=chosen,limit=1] armor.head

kill @e[type=armor_stand,tag=armor_stash,tag=chosen]

tag @s remove me