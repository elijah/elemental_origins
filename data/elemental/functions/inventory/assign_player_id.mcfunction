#
# This function gives a unique player_id (stored
# in a scoreboard) to any player that runs this function.
#
# If the player already has a player_id score, then
# no change happens.
#

# create player_id objective
scoreboard objectives add player_id dummy

# uncomment to debug
# scoreboard objectives setdisplay sidebar player_id

# initialize player_id counter, both global and player's
scoreboard players add $total player_id 0
scoreboard players add @s player_id 0

# if player has player_id = 0, increment counter
execute as @s[scores={player_id=0}] run scoreboard players add $total player_id 1

# if player has player_id = 0, set player_id to counter
execute as @s[scores={player_id=0}] run scoreboard players operation @s player_id = $total player_id
