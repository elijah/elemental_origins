
particle minecraft:glow ~ ~ ~ 0.2 0.2 0.2 0.1 50
execute if entity @s[nbt={SelectedItem:{id:"minecraft:acacia_sapling"}}] run place feature minecraft:acacia ~ ~ ~
execute if entity @s[nbt={SelectedItem:{id:"minecraft:birch_sapling"}}] run place feature minecraft:birch ~ ~ ~
execute if entity @s[nbt={SelectedItem:{id:"minecraft:cherry_sapling"}}] run place feature minecraft:cherry ~ ~ ~
execute if entity @s[nbt={SelectedItem:{id:"minecraft:dark_oak_sapling"}}] run place feature minecraft:dark_oak ~ ~ ~
execute if entity @s[nbt={SelectedItem:{id:"minecraft:jungle_sapling"}}] run place feature minecraft:jungle_tree ~ ~ ~
execute if entity @s[nbt={SelectedItem:{id:"minecraft:oak_sapling"}}] run place feature minecraft:oak ~ ~ ~
execute if entity @s[nbt={SelectedItem:{id:"minecraft:spruce_sapling"}}] run place feature minecraft:spruce ~ ~ ~
execute if entity @s[nbt={SelectedItem:{id:"minecraft:mangrove_propagule"}}] run place feature minecraft:mangrove ~ ~ ~
