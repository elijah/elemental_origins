#!/bin/env bash
DOC_ROOT="/home/minecraft2/web"
TMP_DIR="/tmp/elemental"
TMP_PATH="/tmp/elemental/zip-from-gitlab.zip"
ZIP_PATH="${DOC_ROOT}/elemental_origins.zip"
ZIP_SHA="${DOC_ROOT}/elemental_origins.sha1"
PROJECT_NAME="elemental_origins"
SOURCE_URL="https://0xacab.org/elijah/${PROJECT_NAME}/-/archive/main/${PROJECT_NAME}-main.zip"

cd "~/${PROJECT_NAME}"
git pull
rm -r "$TMP_DIR"
mkdir "$TMP_DIR"
curl "$SOURCE_URL" > "$TMP_PATH"
unzip "$TMP_PATH" -d "$TMP_DIR"
cd "$TMP_DIR/${PROJECT_NAME}-main"
zip -r "../${PROJECT_NAME}.zip" ./*
cd ..
mv "${PROJECT_NAME}.zip" "$ZIP_PATH"
sha1sum "$ZIP_PATH" | cut -f1 -d" " > "$ZIP_SHA"
cd
