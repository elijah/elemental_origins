

# with special tag
give @s netherite_sword{Shadow:1b,display:{Name:'{\"text\":\"Shadow Blade\",\"color\":\"blue\",\"italic\":\"true\"}'},Unbreakable:1,Enchantments:[{id:sharpness,lvl:5},{id:smite,lvl:5},{id:bane_of_arthropods,lvl:5},{id:fire_aspect,lvl:2},{id:knockback,lvl:2},{id:looting,lvl:3},{id:sweeping_edge,lvl:3},{id:vanishing_curse,lvl:1}]}

# give unless already has
execute unless data entity @s {Inventory:[{tag:{Shadow:1b}}]} run give @s netherite_sword{Shadow:1b,display:{Name:'{\"text\":\"Shadow Blade\",\"color\":\"blue\",\"italic\":\"true\"}'},Unbreakable:1,Enchantments:[{id:sharpness,lvl:5},{id:smite,lvl:5},{id:bane_of_arthropods,lvl:5},{id:fire_aspect,lvl:2},{id:knockback,lvl:2},{id:looting,lvl:3},{id:sweeping_edge,lvl:3},{id:vanishing_curse,lvl:1}]}

# destroy if in my inventory
clear @s minecraft:netherite_sword{Shadow:1b} 1

# destroy if in another player's inventory
clear @p[tag=!shadow_mage] minecraft:netherite_sword{Shadow:1b} 1

# destroy if on ground
kill @e[type=item,nbt={Item:{tag:{Shadow:1b}}}]

# destroy in chest??